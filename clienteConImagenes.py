import socket #Para la comunicacion
import threading #Para poder conectar mas de un cliente simultaneo

def recibir_mensaje(cliente_socket):
    while True:
        try:
            data = cliente_socket.recv(1024)
            if data:
                print(data.decode('utf-8'))
            else:
                break
        except Exception as e:
            print(f'Error al recibir mensajes: {e}')
            break

#
host = '127.0.0.1'  # Cambia la dirección IP si el servidor se encuentra en otra máquina de la red
port = 5555  # Utiliza el mismo puerto especificado en el código del servidor


cliente_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


cliente_socket.connect((host, port))
print('Conexión establecida con el servidor.')


receive_thread = threading.Thread(target=recibir_mensaje, args=(cliente_socket,))
receive_thread.start()

while True:
    message = input('Mensaje (Escribe "archivo" para enviar una imagen): ')
    if message.lower() == 'quit':
        break

    try:
        if message.lower() == 'archivo':
            file_path = input('Ruta del archivo de imagen: ')
            with open(file_path, 'rb') as file:
                image_data = file.read()
                cliente_socket.sendall(b'FILE')
                cliente_socket.sendall(image_data)
                print('Imagen enviada exitosamente.')
        else:
            cliente_socket.sendall(message.encode('utf-8'))
    except Exception as e:
        print(f'Error al enviar mensaje: {e}')
        break

cliente_socket.close()
