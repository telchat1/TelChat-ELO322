import socket
import threading

def handle_client(client_socket, client_address):
    print(f'Cliente {client_address} conectado.')

    while True:
        try:
            data = client_socket.recv(1024)
            if data:
                if data == b'FILE':
                    receive_image(client_socket, client_address)
                else:
                    print(f'Mensaje recibido de {client_address}: {data.decode("utf-8")}')
                    broadcast_message(data, client_address)
            else:
                print(f'Cliente {client_address} desconectado.')
                break
        except Exception as e:
            print(f'Error al recibir datos del cliente {client_address}: {e}')
            break

def broadcast_message(message, sender_address):
    for client in clients:
        client_socket, client_address = client
        if client_address != sender_address:
            try:
                client_socket.sendall(message)
            except Exception as e:
                print(f'Error al enviar mensaje a {client_address}: {e}')
                client_socket.close()
                clients.remove(client)

def receive_image(client_socket, sender_address):
    try:
        #recibir y guardar la imagen
        file_path = f'image_from_{sender_address}.jpg'
        with open(file_path, 'wb') as file:
            while True:
                data = client_socket.recv(1024)
                if data == b'END':
                    break
                file.write(data)
        print(f'Imagen recibida de {sender_address}. Guardada en: {file_path}')

        #confirmacion para el cliente
        client_socket.sendall(b'Imagen recibida correctamente.')
    except Exception as e:
        print(f'Error al recibir la imagen de {sender_address}: {e}')

#configuración del servidor
host = '127.0.0.1'
port = 5555


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


server_socket.bind((host, port))

# Escuchar conexiones entrantes
server_socket.listen(5)
print('Servidor de chat en red local iniciado.')

clients = []

while True:
    client_socket, client_address = server_socket.accept()
    clients.append((client_socket, client_address))

    client_thread = threading.Thread(target=handle_client, args=(client_socket, client_address))
    client_thread.start()
